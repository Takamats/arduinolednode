$(function() {
    var socket = io.connect();
    // connect by WebSocket
    socket.on('connect', function(msg) {
        console.log("connect");
    });

    // switch ON all LED
    $("#led").click(function(event) {
        var msg = new Object();
        msg.led = 'ON';
        socket.emit('message', {
            value: msg
        });
    });

    // switch OFF all LED
    $("#led-off").click(function(event) {
        var msg = new Object();
        msg.led = 'OFF';
        socket.emit('message', {
            value: msg
        });
    });


    // LED Pattern name
    var pattern = ["ON", "RED", "GREEN", "BLUE"];

    // LED Pattern default value
    var patternOrd = 0;

    // Switch LED pattern
    $("#pattern-btn").click(function(event) {
        if (patternOrd == pattern.length) {
            patternOrd = 0;
        }
        var msg = new Object();
        msg.led = pattern[patternOrd];
        socket.emit('message', {
            value: msg
        });
        console.log("pattern: " + msg.led);
        patternOrd++;
    });

    function SendMsg() {
        var msg = $('#message').val();
        // Send message
        socket.emit('message', {
            value: msg
        });
        $('#message').val('');
    }
});

var shakeState = 0;
window.addEventListener('devicemotion', function(event) {

    $('#x').text(event.acceleration.x * 10);
    $('#y').text(event.acceleration.y * 10);
    $('#z').text(event.acceleration.z * 10);

    var x = parseInt(event.accelerationIncludingGravity.x * 10);

    // Change background color
    if (x < -50 && shakeState == 0) {
        $('.deg').text("ON");
        shakeState = 1;
        $("#shake-btn").click();
        $("body").css("background-color", "#fff");
    } else if (x > 20 && shakeState == 1) {
        $('.deg').text("OFF");
        $("body").css("background-color", "#000");
        shakeState = 0;
        $("#pattern-btn").click();
    }

    $('#xg').text(event.accelerationIncludingGravity.x * 10);
    $('#yg').text(event.accelerationIncludingGravity.y * 10);
    $('#zg').text(event.accelerationIncludingGravity.z * 10);

    $('#a').text(event.rotationRate.alpha * 10);
    $('#b').text(event.rotationRate.beta * 10);
    $('#g').text(event.rotationRate.gamma * 10);
});