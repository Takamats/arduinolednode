/**
 * Module dependencies.
 */

var express = require('express'),
    routes = require('./routes'),
    path = require('path'),
    serialport = require('serialport');

var app = express(),
    http = require('http'),
    server = http.createServer(app),
    io = require('socket.io').listen(server);

server.listen(3000, "192.168.2.2");
// Serial Port
var portName = '/dev/cu.usbmodem1411'; // Mac
var sp = new serialport.SerialPort(portName, {
    baudRate: 115200,
    dataBits: 8,
    parity: 'none',
    stopBits: 1,
    flowControl: false,
    parser: serialport.parsers.readline("\n")
});

app.configure(function() {
    app.set('port', process.env.PORT || 3000);
    app.set('views', __dirname + '/views');
    app.set('view engine', 'ejs');
    app.use(express.favicon());
    app.use(express.logger('dev'));
    app.use(express.bodyParser());
    app.use(express.methodOverride());
    app.use(express.cookieParser('your secret here'));
    app.use(express.session());
    app.use(app.router);
    app.use(express.static(path.join(__dirname, 'public')));
});

app.configure('development', function() {
    app.use(express.errorHandler());
});

app.get('/', routes.index);

// wait web client connection
server.listen(app.get('port'));

// connection web clients
io.sockets.on('connection', function(socket) {
    console.log("connection");

    // Received data
    socket.on('message', function(data) {
        console.log(data.value.led);
        // // Send web clients
        socket.broadcast.json.emit('message', {
            value: data.value //msgを送信?
        });

        console.log('Client sent us: ' + data.value.led);
        sp.write(data.value.led, function(err, bytesWritten) {
            console.log('bytes written: ', bytesWritten);
        });
    });

    // disconnected web client
    socket.on('disconnect', function() {
        console.log("disconnect");
    });

    // data for Serial port
    socket.on('sendSerial', function(data) {
        var receive = JSON.stringify(data);
        console.log('Client sent us: ' + receive);
        sp.write(receive, function(err, bytesWritten) {
            console.log('bytes written: ', bytesWritten);
        });
    });
});

// data from Serial port
sp.on('data', function(input) {

    var buffer = new Buffer(input, 'utf8');
    var jsonData;
    try {
        jsonData = JSON.parse(buffer);
        console.log('led: ' + jsonData.led);
        console.log('led: ' + jsonData.analog);
    } catch (e) {
        // Ignore if receive wrong data
        return;
    }
    // Send web clients
    io.sockets.json.emit('message', {
        value: jsonData
    });
});

sp.on('close', function(err) {
    console.log('port closed');
});

sp.on('open', function(err) {
    console.log('port opened');
});