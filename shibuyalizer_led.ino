#include <Adafruit_NeoPixel.h>
#define MAX_VAL 100  // 0 to 255 for brightness
#define DELAY_TIME 10 
#define LED  13
#define analogLED  A0
#define ledRed  3
#define ledGreen  5
#define ledBlue  6
Adafruit_NeoPixel strip = Adafruit_NeoPixel(20, 8, NEO_GRB + NEO_KHZ800);

int value; 
char trans[20]; // Array for send charachter
char recv[10]; // Array for receive charachter

void setup() {
  pinMode(LED, OUTPUT);
  pinMode(ledRed, OUTPUT);
  pinMode(ledGreen, OUTPUT);
  pinMode(ledBlue, OUTPUT);
  strip.begin();
  strip.show(); // Initialize all pixels to 'off'
  Serial.begin(115200);
}

void loop() {
  // Data from serial port
  memset(recv, 0, 10);
  char *p = &recv[0];
  recvStr(p);
  if (strcmp("ON", p) == 0) {
    digitalWrite(LED, HIGH);
    analogWrite(analogLED, 150);
    analogWrite(ledRed, 255);
    analogWrite(ledGreen, 255);
    analogWrite(ledBlue, 255);
    colorWipe(strip.Color(100, 100, 100), DELAY_TIME); // White
  } else if (strcmp("OFF", p) == 0) {
    digitalWrite(LED, LOW);
    analogWrite(analogLED, 0);
    analogWrite(ledRed, 0);
    analogWrite(ledGreen, 0);
    analogWrite(ledBlue, 0);
    colorWipe(strip.Color(0, 0, 0), DELAY_TIME); // Black
  } else if (strcmp("RED", p) == 0){
    analogWrite(ledRed, 255);
    analogWrite(ledGreen, 0);
    analogWrite(ledBlue, 0);
    colorWipe(strip.Color(255, 0, 0), DELAY_TIME); // Red
  } else if (strcmp("GREEN", p) == 0){
    analogWrite(ledRed, 0);
    analogWrite(ledGreen, 255);
    analogWrite(ledBlue, 0);
    colorWipe(strip.Color(0, 255, 0), DELAY_TIME); // Green
  } else if (strcmp("BLUE", p) == 0){
    analogWrite(ledRed, 0);
    analogWrite(ledGreen, 0);
    analogWrite(ledBlue, 255);
    colorWipe(strip.Color(0, 0, 255), DELAY_TIME); // Blue
  }
  //  Make Json data
  memset(trans, 0, 20);
  char *json = &trans[0];
  sprintf(json, "{\"led\":%d, \"analog\":%d}", digitalRead(LED), analogRead(analogLED));
  Serial.println(json);
}

// Receive data
void recvStr(char *buf)
{
  int i = 0;
  char c;
  while (Serial.available()) {
    c = Serial.read();
    buf[i] = c;
    i++;
  }
  delay(10);
}

// Fill the dots one after the other with a color
void colorWipe(uint32_t c, uint8_t wait) {
  for(uint16_t i=0; i<strip.numPixels(); i++) {
    strip.setPixelColor(i, c);
    strip.show();
  }
}

